base:
  'role:builder':
    - match: pillar
    - nfs
    - mount
    - php
    - php.mysql
    - php.mysqlnd
    - apache
    - apache.config
    - apache.vhosts.standard
    - apache.no_default_vhost
    - mysql
    - mysql.config
    - mysql.client
  'role:seeder':
    - match: pillar
    - nfs
    - php
    - php.mysql
    - php.mysqlnd
    - apache
    - apache.config
    - apache.vhosts.standard
    - apache.no_default_vhost
    - mysql
    - mysql.config
    - mysql.client
    - mount
    - wordpress
  'role:node':
    - match: pillar
    - mount
