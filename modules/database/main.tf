resource "aws_security_group" "rds-in" {
  name   = "WordPress - RDS"
  vpc_id = var.vpc_id

  ingress {
    # TLS (change to whatever ports you need)
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Terraform   = "true"
    Environment = var.environment_name
  }
}

module "rds" {
  source                  = "terraform-aws-modules/rds/aws"
  version                 = "2.5.0"
  identifier              = "ha-wordpress-db"
  engine                  = "mysql"
  engine_version          = "5.7.22"
  instance_class          = "db.t2.micro"
  allocated_storage       = 5
  name                    = "demodb"
  username                = "user"
  password                = "YourPwdShouldBeLongAndSecure!"
  port                    = "3306"
  maintenance_window      = "Mon:00:00-Mon:03:00"
  backup_window           = "03:30-06:00"
  backup_retention_period = "7"

  db_subnet_group_name = var.db_subnet_group
  family               = "mysql5.7"
  major_engine_version = "5.7"

  final_snapshot_identifier = "ha-wordpress-db"
  deletion_protection       = false
  vpc_security_group_ids    = [aws_security_group.rds-in.id]

  tags = {
    Terraform   = "true"
    Environment = var.environment_name
  }
}

