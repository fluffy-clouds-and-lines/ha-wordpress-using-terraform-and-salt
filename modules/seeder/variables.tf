variable "public_subnets" {
  type = list(string)
}

variable "environment_name" {
}

variable "ec2_private_key" {
}

variable "vpc_id" {
}

variable "db_endpoint" {
}

variable "db_user" {
}

variable "db_pass" {
}

variable "efs_host" {
}

variable "efs_mounts" {
  type = list(string)
}

variable "wp_admin_user" {
}

variable "wp_admin_password" {
}

variable "wp_site_title" {
}

variable "wp_site_email" {
}

variable "http_protocol" {
}

variable "dns_root" {
}

