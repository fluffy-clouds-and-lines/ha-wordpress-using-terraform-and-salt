data "aws_ami" "base_image" {
  owners      = ["099720109477"]
  most_recent = true

  filter {
    name   = "name"
    values = ["*ubuntu-bionic-18.04*"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_security_group" "sg-wordpress-seeder" {
  name   = "WordPress Seeder"
  vpc_id = var.vpc_id

  ingress {
    # TLS (change to whatever ports you need)
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    # TLS (change to whatever ports you need)
    from_port = 80
    to_port   = 80
    protocol  = "tcp"

    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "wordpress_seeder" {
  ami                         = data.aws_ami.base_image.id
  instance_type               = "t2.micro"
  key_name                    = "wordpressha"
  security_groups             = [aws_security_group.sg-wordpress-seeder.id]
  subnet_id                   = var.public_subnets[0]
  associate_public_ip_address = true

  tags = {
    "Name" = "wordpress-seeder"
  }

  provisioner "salt-masterless" {
    connection {
      host        = coalesce(self.public_ip, self.private_ip)
      type        = "ssh"
      user        = "ubuntu"
      private_key = var.ec2_private_key
    }

    local_state_tree    = "./salt_tree/srv/salt"
    remote_state_tree   = "/srv/salt"
    local_pillar_roots  = "./salt_tree/srv/pillar"
    remote_pillar_roots = "/srv/pillar"
    salt_call_args      = "pillar='{\"role\":\"seeder\"}'"
  }
}

data "template_file" "wordpress_config" {
  template = file("./salt_tree/srv/pillar/wordpress.tmpl")

  vars = {
    dbhost            = var.db_endpoint
    dbuser            = var.db_user
    dbpass            = var.db_pass
    dns_root          = var.dns_root
    http_protocol     = var.http_protocol
    wp_admin_user     = var.wp_admin_user
    wp_admin_password = var.wp_admin_password
    wp_site_title     = var.wp_site_title
    wp_site_email     = var.wp_site_email
  }
}

data "template_file" "apache_config" {
  template = file("./salt_tree/srv/pillar/apache.tmpl")

  vars = {
    dns_root = var.dns_root
  }
}

resource "local_file" "save_wordpress_config" {
  content  = data.template_file.wordpress_config.rendered
  filename = "./salt_tree/srv/pillar/wordpress.sls"
}

resource "local_file" "save_apache_config" {
  content  = data.template_file.apache_config.rendered
  filename = "./salt_tree/srv/pillar/apache.sls"
}

data "template_file" "efs_config" {
  template = file("./salt_tree/srv/salt/mount.tmpl")

  vars = {
    efs_host = var.efs_host
  }
}

resource "local_file" "save_efs_config" {
  content  = data.template_file.efs_config.rendered
  filename = "./salt_tree/srv/salt/mount.sls"
}

