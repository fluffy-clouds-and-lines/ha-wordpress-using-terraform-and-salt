output "efs_host" {
  value = aws_efs_file_system.wproot-efs.dns_name
}

output "efs_mounts" {
  value = aws_efs_mount_target.wproot-efs-mounts.*.dns_name
}

