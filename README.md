# HA Wordpress using Terraform and Salt
A full suite to install a HA WordPress installation on AWS using Terraform and Packer.

Read more @ https://fluffycloudsandlines.blog/deploying-ha-wordpress-on-aws-using-terraform-and-salt/.